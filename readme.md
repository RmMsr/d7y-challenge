# Example application
    
A minimal demo for multiple websites inside a kubernetes cluster.

## Preparation

You need an kubernetes cluster. Ideally with a running Ingress controller. The rest
of this documentation will assume you use a local Kubernetes in Docker.

### Requirements

* Docker (only for Kubernetes in Docker)
* kubectl

In case you don't have a k8s cluster already, you can use the supplied scripts to 
boot a local [kind (Kubernetes in Docker)](https://kind.sigs.k8s.io/).

    develop/kind/manage.sh buildup

In order to use your local k8s please execute the command printed during cluster
creation. It's similar to this:

    export KUBECONFIG=$HOME/.kube/kind-config-rmmsr-d7y

## Deployment

Initially the container needs to be build:

    develop/build.sh

In order to deploy the application execute this command:

    develop/deploy.sh buildup

All components will be created in the `default` namespace of your k8s. You can now
access the content using any HTTP client:

    curl -H "Host: challenge.local" http://localhost/

Note: In order to access the content without overwriting the host name, you'll need
some other mechanism to resolve the host name (e.g. an entry in /etc/hosts).

### ToDo

* Expose http server metrics (request status, duration, sizes)
* Ensure webserver runs as non-root user
* Explicit deploy a container version to ensure that the expected version is running.
  Instead of using the ` latest` image tag
* Add liveness probe for webserver container
* Specify Pod Disruption Budget to support maintenance without downtime

## License

This software is made available under the terms of the
[Unlicense](https://unlicense.org/): Use it in any way you like.