#!/usr/bin/env sh

set -e

project_dir=$(cd -- "$(dirname -- "$0")/../.." && pwd -P)
kind_name=rmmsr-d7y
KUBECONFIG=${KUBECONFIG:-$HOME/.kube/kind-config-${kind_name}}

kind_exists() {
  kind get nodes --name "$kind_name" 2> /dev/null | grep -qc "$kind_name"
}

setup_kubeconfig() {
  export KUBECONFIG
  kind export kubeconfig --name "$kind_name"
  kubectl cluster-info

  echo
  echo CONFIGURATION:
  echo Use the lokal K8s cluster with kubectl:
  echo export "KUBECONFIG=$KUBECONFIG"
}

setup_ingress(){
  kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.27.0/deploy/static/mandatory.yaml
  kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/nginx-0.27.0/deploy/static/provider/baremetal/service-nodeport.yaml
  kubectl patch deployments -n ingress-nginx nginx-ingress-controller -p '{"spec":{"template":{"spec":{"containers":[{"name":"nginx-ingress-controller","ports":[{"containerPort":80,"hostPort":80},{"containerPort":443,"hostPort":443}]}],"nodeSelector":{"ingress-ready":"true"},"tolerations":[{"key":"node-role.kubernetes.io/master","operator":"Equal","effect":"NoSchedule"}]}}}}'
}

buildup_kind() {
  if kind_exists; then
    echo Kind "$kind_name" already exists
  else
    kind create cluster --name "$kind_name" --config "$project_dir/develop/kind/config.yaml" --wait 300s
  fi

  container="${kind_name}-control-plane"
  if [ -z "$(docker container ls --filter "name=${container}" --quiet)" ]; then
    docker container start "$container"
  fi

  setup_kubeconfig

  setup_ingress
}

teardown_kind() {
  kind delete cluster --name "$kind_name"
}

case $1 in
  buildup)
    buildup_kind
    ;;
  teardown)
    teardown_kind
    ;;
  info)
    setup_kubeconfig
    ;;
  *)
    echo "Specify buildup, teardown or info as parameter"
esac
