#!/usr/bin/env sh

set -e

project_dir=$(cd -- "$(dirname -- "$0")/.." && pwd -P)
kind_name=rmmsr-d7y
image=rmmsr-d7y:latest

buildup() {
  kind load docker-image --name "$kind_name" "$image"
  kubectl apply -f "$project_dir/k8s/"
}

teardown() {
  kubectl delete -f "$project_dir/k8s/"
}

case $1 in
  buildup)
    buildup
    ;;
  teardown)
    teardown
    ;;
  *)
    echo "Specify buildup or teardown as parameter"
esac

