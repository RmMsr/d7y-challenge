#!/usr/bin/env sh

set -e

project_dir=$(cd -- "$(dirname -- "$0")/.." && pwd -P)
image=rmmsr-d7y

docker build \
  -f "$project_dir/build/Dockerfile" \
  -t "$image" \
  "$project_dir"