package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

// Greeter responds with salutation
type Greeter struct {
	salutation string
}

// Super simple HTTP server
func main() {
	message := os.Getenv("MESSAGE")
	if len(message) == 0 {
		panic("No env MESSAGE given")
	}

	http.Handle("/", Greeter{salutation: message})

	log.Println("Starting webserver...")
	if http.ListenAndServe(":8080", nil) != nil {
		panic("Could not open listening socket")
	}
}

// ensure we implement the Handler interface
var _ http.Handler = Greeter{}

// ServeHTTP answers requests
func (g Greeter) ServeHTTP(w http.ResponseWriter, _ *http.Request) {
	log.Println("Serving request")
	fmt.Fprint(w, g.salutation)
}
