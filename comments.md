# Things to consider

## Monitoring

As a minimum the error rate and request durations should be measured at the
loadbalancer (Ingress) level. This could be done by exporting the metrics of the
Ingress proxy and consume it with Prometheus. Based on this metrics some alarms should
be created.

To ensure that the service is constantly usable by the consumers an external check
should be implemented. This could be a simple GET request every 10 sec as healths check.

### Possible alarms

* Error rate exceeds 5% within 30 sec
* Request rate exceeds 1000 / min
* Request duration exceeds 100ms (95 percentile)
* Pods are restarting more often than 3 times within 3 min
* Pods are in pending state for longer than 3 min
* Absence of the external health check for more than 30 sec

### Logging

It might be useful to capture the output of the web servers and other components in a log
aggregator. This can aid with error analysis.

## Scaling

The web servers are very simple but support some concurrent connections. For more
parallel requests the number of replicas can be easily increased. This can happen fully
automated.

Performance of a single request can be improved on several levels. A HTTP cache would
serve the pages almost instant from memory. Once the generation of the websites get's 
more time consuming, a worker pool might optimize resource utilization within an
container.

## Operation model

Serving static pages should not be implemented with custom software. Depending on the
concrete case it maybe should not even run withing kubernetes. Ideally this is
outsourced to a cloud service. If business logic is needed to serve the content, 
a serverless service could be considered.

In general the use of a hosted solution will improve reliability, performance and
maintainability. The complexity of dealing with the providers API might be noticeable
but will remain predictable low. nd pay out quickly. The custom solution will produce
more work that is less predictable due to varying knowledge of the engineers, unplanned
security updates, etc.

## Protection

* Rate limiting would protect from some denial of service attacks or misconfigurations
* All software parts should periodically be evaluated to be up to date and without
  known errors
* Access within Kubernetes should be limited using NetworkPolicies